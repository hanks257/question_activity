// app.js
import {
  sendPost
} from './utils/request.js'
import {
  codeLoginApi
} from './utils/config.js'
App({
  sendPost,

  onLaunch() {
    /**
     * 这个框架，尽最大限度保证openid保存在了storage里边，除了codeLogin接口，
     * 其他接口都会默认携带openid参数，每个页面data里边不需要再保存openid，
     * openid默认就是当前用户自己的openid
    */
     if (!wx.getStorageSync('openid')) {
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 用户基本信息
          sendPost(codeLoginApi, {
            code: res.code
          }).then(res => {
            wx.setStorageSync('openid', res.data.openid)
            if (this.userBaseLoginCallback) {
              this.userBaseLoginCallback(res)
            }
          })
        }
      })
    }

    //以下为版本更新的代码
    const updateManager = wx.getUpdateManager()

    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本小程序已经准备好，是否重启？',
        success(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
  },
  globalData: {
    userInfo: null
  }
})