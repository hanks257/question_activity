
const envVersion = wx.getAccountInfoSync().miniProgram.envVersion

//登录接口的地址
const codeLoginApi = 'wechat/codeLogin'

const baseUrl = function () {
	if ( envVersion === 'develop') {//开发版
		console.log('开发环境')
		return 'http://localhost:8888/'
    // return'https://xxx/miniprogram/'
  } else if (envVersion === 'trial') { //体验版
    console.log('体验环境')
    return'https://xxx/miniprogram/'
  } else if (envVersion === 'release') { //生产版
    console.log('生产环境')
    return 'https://xxx/miniprogram/'
  }
}

let whiteApi = ['wechat/userBaseLogin'] //不需要token的白名单-**loginApi为登陆接口**


module.exports = {
  baseUrl,
  whiteApi,
  codeLoginApi
}
